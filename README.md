# Simple bootstrap for linking and using Rust shared libraries with C++

__Disclaimer:__ This is done by a C++ programmer... My goal is to port some C++ to Rust, so I made this bootstrap. The intent is to keep C++ code, then replace small parts of it without a rewrite from scratch.

## Usage for a shared lib 'rust_lib'
1) Use `add_rust_lib(rust_lib)` to add the library
2) Use `add_rust_test(rust_lib)` to add tests
3) Use `target_link_rust_library(runner rust_lib)` to link the library with a C++ target 'runner'

See `CMakeFile.txt` files for more details.

## This is not exhaustively tested
Seems to work with CMake generators for:
- Visual Studio 2019 (Windows)
- Visual Studio Code (Windows) (in app generation)
- Makefile (Linux)
- Ninja (Linux, single-configuration build, not tested with multi-configuration builds)

## For RelWithDebInfo configurations the rust lib Cargo.toml file must include:
```
[profile.release-with-debug-info]
inherits = "release"
debug = true
```

## The sample 'rust_lib'

This sample uses a custom `build.rs` file to generate a header `rust_lib.h` using `cbindgen` crate. Right now, most of build time is spend to pull and build the `cbindgen` crate. But not having to write the binding is great.

## TODO: add MinSizeRel build configuration
This needs to be done, should be similar to RelWithDebInfo but need to figure out rust options (I never used this configuration in production).
