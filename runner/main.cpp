#include <iostream>
#include <rust_lib.h>

#define UNUSED(x) (void)(x)

int main(int argc, char *argv[])
{
    UNUSED(argc);
    UNUSED(argv);

    std::cout << "C++ code" << std::endl;
    rust_lib::some_fun();
    return 0;
}
