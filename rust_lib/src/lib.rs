fn some_other_fun(x: u32, y: u32) {
    println!("rust function called {}", x + y);
}

/// Some rust function
#[no_mangle]
pub extern "C" fn some_fun() {
    some_other_fun(10, 20);
}

#[cfg(test)]
mod tests {
    #[test]
    fn it_works() {
        let mut i = 4;
        assert_eq!(i, 4);
        i = 5;
        assert_eq!(i, 5);
    }
}
