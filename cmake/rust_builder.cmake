#
# Usage for a shared lib 'rust_lib'
#   1) add_rust_lib(rust_lib)  # add the library
#   2) add_rust_test(rust_lib) # add tests
#   3) target_link_rust_library(runner rust_lib) # link the libary with a CPP target 'runner'
#
# This is not exhaustively tested seems to work with cmakes generators for:
#   - Visual Studio 2019 (Windows)
#   - Visual Studio Code (Windows) (in app generation)
#   - Makefiles (Linux)
#   - Ninja (Linux, single-configuration build, not tested with multi-configuration builds)
#
# For RelWithDebInfo configurations the rust lib Cargo.toml file must include:
#
# [profile.release-with-debug-info]
# inherits = "release"
# debug = true
#

macro(add_rust_lib RUST_TARGET)
    # Sadly when using Make builds, the --config option is silently ignored, and when using multi-configuration
    # build tools like Visual Studio, the CMAKE_BUILD_TYPE is also silently ignored.
    get_property(IS_MULTI_CONFIG GLOBAL PROPERTY GENERATOR_IS_MULTI_CONFIG)
    if (${IS_MULTI_CONFIG})
        set(CARGO_BUILD_DIR "$<$<CONFIG:Debug>:debug>$<$<CONFIG:RelWithDebInfo>:release-with-debug-info>$<$<CONFIG:Release>:release>")
        set(CARGO_TARGET_DIR "${CMAKE_CURRENT_BINARY_DIR}/${CARGO_BUILD_DIR}")
        set(CARGO_CMD cargo build $<$<CONFIG:Release>:--release>$<$<CONFIG:RelWithDebInfo>:--profile=release-with-debug-info>)
        MESSAGE(STATUS "GENERATOR_IS_MULTI_CONFIG")
    else()
        if (NOT CMAKE_BUILD_TYPE)
            set (CMAKE_BUILD_TYPE Debug CACHE STRING "" FORCE)
            MESSAGE(STATUS "No CMAKE_BUILD_TYPE forced to ${CMAKE_BUILD_TYPE}")
        else()
            MESSAGE(STATUS "CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}")
        endif()

        if (CMAKE_BUILD_TYPE STREQUAL "Debug")
            set(CARGO_BUILD_DIR "debug")
            set(CARGO_CMD cargo build)
        elseif (CMAKE_BUILD_TYPE STREQUAL "RelWithDebInfo")
            set(CARGO_BUILD_DIR "release-with-debug-info")
            set(CARGO_CMD cargo build --profile=release-with-debug-info)
        elseif (CMAKE_BUILD_TYPE STREQUAL "Release")
            set(CARGO_BUILD_DIR "release")
            set(CARGO_CMD cargo build --release)
        else()
            MESSAGE(FATAL_ERROR "Unsupported CMAKE_BUILD_TYPE ${CMAKE_BUILD_TYPE}")
        endif()
    endif()

    set(CARGO_TARGET_DIR "${CMAKE_CURRENT_BINARY_DIR}/${CARGO_BUILD_DIR}")

    MESSAGE(VERBOSE "CARGO_CMD ${CARGO_CMD}")
    MESSAGE(VERBOSE "CARGO_TARGET_DIR ${CARGO_TARGET_DIR}")

    # byproducts need to be de defined for Ninja
    if (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        set(RUST_BYPRODUCTS ${RUST_TARGET}.dll ${RUST_TARGET}.dll.lib)
    else()
        set(RUST_BYPRODUCTS lib${RUST_TARGET}.so)
    endif()

    add_custom_target(${RUST_TARGET} ALL
        COMMENT "Compiling rust_lib module"
        BYPRODUCTS ${RUST_BYPRODUCTS}
        COMMAND ${CMAKE_COMMAND} -E echo "CARGO_TARGET_DIR is ${CARGO_TARGET_DIR}"
        COMMAND ${CMAKE_COMMAND} -E env CARGO_TARGET_DIR=${CMAKE_CURRENT_BINARY_DIR} ${CARGO_CMD}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})

    if (${CMAKE_SYSTEM_NAME} MATCHES "Windows")
        set_target_properties(${RUST_TARGET} PROPERTIES LIBRARY_SHARED_FILE ${RUST_TARGET}.dll)
        set_target_properties(${RUST_TARGET} PROPERTIES LIBRARY_LIB_FILE ${RUST_TARGET}.dll.lib)

        add_custom_command(TARGET ${RUST_TARGET} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CARGO_TARGET_DIR}/${RUST_TARGET}.dll ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.dll
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CARGO_TARGET_DIR}/${RUST_TARGET}.dll.lib ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.dll.lib)
        set_property(
                TARGET ${RUST_TARGET}
                APPEND
                PROPERTY ADDITIONAL_CLEAN_FILES
                ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.dll
                ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.dll.lib
                ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.h
                ${CARGO_TARGET_DIR}
        )
    else()
        set_target_properties(${RUST_TARGET} PROPERTIES LIBRARY_SHARED_FILE lib${RUST_TARGET}.so)
        set_target_properties(${RUST_TARGET} PROPERTIES LIBRARY_LIB_FILE lib${RUST_TARGET}.so)

        add_custom_command(TARGET ${RUST_TARGET} POST_BUILD
            COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CARGO_TARGET_DIR}/lib${RUST_TARGET}.so ${CMAKE_CURRENT_BINARY_DIR}/lib${RUST_TARGET}.so)
        set_property(
                TARGET ${RUST_TARGET}
                APPEND
                PROPERTY ADDITIONAL_CLEAN_FILES
                    ${CMAKE_CURRENT_BINARY_DIR}/lib${RUST_TARGET}.so
                    ${CMAKE_CURRENT_BINARY_DIR}/${RUST_TARGET}.h
                    ${CARGO_TARGET_DIR}
        )
    endif()

    set_target_properties(${RUST_TARGET} PROPERTIES LOCATION ${CMAKE_CURRENT_BINARY_DIR})

    file(GLOB_RECURSE RUST_SOURCES ${CMAKE_CURRENT_SOURCE_DIR}/src/*.rs)

    MESSAGE(VERBOSE "RUST_SOURCES ${RUST_SOURCES}")
    source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR} FILES ${RUST_SOURCES})

    # workaround because target_sources does not work with Make
    set_property(TARGET ${RUST_TARGET} APPEND PROPERTY SOURCES
        ${RUST_SOURCES}
        "${CMAKE_CURRENT_SOURCE_DIR}/Cargo.toml"
        "${CMAKE_CURRENT_SOURCE_DIR}/build.rs")
endmacro()

macro(add_rust_test RUST_TARGET)
    add_test(NAME ${RUST_TARGET}_test
        COMMAND cargo test
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR})
endmacro()

macro(target_link_rust_library CPP_TARGET RUST_LIB)
    get_target_property(RUST_LIB_DIR ${RUST_LIB} LOCATION)
    get_target_property(RUST_SHARED_FILE ${RUST_LIB} LIBRARY_SHARED_FILE)
    get_target_property(RUST_LIB_FILE ${RUST_LIB} LIBRARY_LIB_FILE)

    target_include_directories(${CPP_TARGET} PRIVATE ${RUST_LIB_DIR})
    target_link_libraries(${CPP_TARGET} ${RUST_LIB_DIR}/${RUST_LIB_FILE})

    add_custom_command(TARGET ${CPP_TARGET} POST_BUILD
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${RUST_LIB_DIR}/${RUST_SHARED_FILE} $<TARGET_FILE_DIR:${CPP_TARGET}>/${RUST_SHARED_FILE}
    )
    set_property(TARGET ${CPP_TARGET}
                 APPEND
                 PROPERTY ADDITIONAL_CLEAN_FILES
                     $<TARGET_FILE_DIR:${CPP_TARGET}>/${RUST_SHARED_FILE}
    )
    add_dependencies(${CPP_TARGET} ${RUST_LIB})
endmacro()
